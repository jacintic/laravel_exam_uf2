<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;


class CompraRequest extends FormRequest
{
    //protected $redirectRoute = 'post.create' //ruta definida en alguno de los archivos de la carpeta routes
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=> 'required',
            'email'=> 'required',
            'direccion'=> 'required',
            'password'=> 'required',
            'password_confirmation'=> 'required',
            'foto'=> 'required',

        ];
    }

    public function messages()
    {
        return [
           'name.required'=>'Please introduce your :attribute',
           'email.required'=>'Please introduce your :attribute',
           'direccion.required'=>'Please introduce your :attribute',
           'password.required'=>'Please introduce your :attribute.',
           'password_confirmation.required'=>'Please introduce your :attribute',
           'foto.required'=>'Please upload your :attribute',
        ];
    }
    public function attributes()
    {
        return [
            'name'=> 'username',
            'email'=> 'email',
            'direccion'=> 'directon',
            'password'=> 'password',
            'password_confirmation'=> 'password confirmation',
            'foto'=> 'profile picture',
        ];
    }
    /**
     *  AJAX Response 
     */
    public function response(array $errors)
    {
        if ($this->expectsJson()) {
            return new JsonResponse($errors, 422);
        }
        return $this->redirector->to($this->getRedirectUrl())
            ->withInput($this->except($this->dontFlash))
            ->withErrors($errors, $this->errorBag);
    }
}
